import banner from '../assets/image/autenticacao/banner.png';
import logo from '../assets/image/autenticacao/logo.png';
import iconeFacebook from '../assets/image/autenticacao/icone_face.png';
import iconeGoogle from '../assets/image/autenticacao/icone_google.png';
import iconeEmail from '../assets/image/autenticacao/icone_email.png';

import recycleDinnome from '../assets/image/termoUso/RecycleDinnome.png';

import imgEntrarContato from '../assets/image/posCadastro/imgEntrarContato.png';
import imgNormasComu from '../assets/image/posCadastro/imgNormasComu.png';
import imgProntoComecar from '../assets/image/posCadastro/imgProntoComecar.png';
import imgPermitirAcesso from '../assets/image/posCadastro/imgPermitirAcesso.png';

import casaiCone from '../assets/image/mapas/casaIcon.png';
import imgAluminio from '../assets/image/mapas/materiais/aluminio.png';
import imgPapel from '../assets/image/mapas/materiais/papel.png';
import imgPlastico from '../assets/image/mapas/materiais/plastico.png';
import imgVidro from '../assets/image/mapas/materiais/vidro.png';

import imgCalendario from '../assets/image/agendarColeta/iconeCalendario.png';
import imgRelogio from '../assets/image/agendarColeta/iconeRelogio.png';

export const imagens = {
  banner,
  logo,
  iconeFacebook,
  iconeGoogle,
  iconeEmail,
  recycleDinnome,
  imgEntrarContato,
  imgNormasComu,
  imgProntoComecar,
  imgPermitirAcesso,
  casaiCone,
  imgAluminio,
  imgPapel,
  imgPlastico,
  imgVidro,
  imgCalendario,
  imgRelogio,
};
