import React, {useState, useEffect} from 'react';
import {StyleSheet, View} from 'react-native';
import {format} from 'date-fns';
import pt from 'date-fns/locale/pt';

import DataPicket from './datePickerInput';

import {
  Container,
  TextoLabelPlaceholder,
  TextoLabelData,
  Labeltop,
} from './styles';

export default function DatePicker() {
  const [show, setShow] = useState(false);
  const [date, setDate] = useState(new Date(1598051730000));
  const [dataFormatada, setDataFormatada] = useState('');

  const style = StyleSheet.create({
    labelTop: {
      transform: [{translateY: -10}],
    },
  });

  function formatarData(data) {
    const formatacao = format(data, "dd'/'MM'/'yyyy", {locale: pt});
    setDataFormatada(formatacao);
  }

  useEffect(() => {
    formatarData(date);
  }, [date]);

  return (
    <>
      <Container>
        {date.getDay().toString() === '5' ? (
          <></>
        ) : (
          <View style={style.labelTop}>
            <Labeltop>Data de Nascimento</Labeltop>
          </View>
        )}

        {date.getDay().toString() === '5' ? (
          <TextoLabelPlaceholder onPress={() => setShow(!show)}>
            Data de Nascimento
          </TextoLabelPlaceholder>
        ) : (
          <TextoLabelData onPress={() => setShow(!show)}>
            {dataFormatada}
          </TextoLabelData>
        )}
      </Container>
      <DataPicket show={show} setShow={setShow} date={date} setDate={setDate} />
    </>
  );
}
