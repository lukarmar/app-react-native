import React from 'react';
import {View, Platform} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';

export default function DatePicker({show, setShow, date, setDate, mode}) {
  // const [date, setDate] = useState(new Date(1598051730000));

  function onChange(event, selectedDate) {
    const currentDate = selectedDate || date;
    setShow(Platform.OS === 'ios');
    setDate(currentDate);
  }

  return (
    <View>
      {show && (
        <DateTimePicker
          testID="dateTimePicker"
          timeZoneOffsetInMinutes={0}
          value={date}
          mode={mode}
          is24Hour={true}
          display="default"
          onChange={onChange}
        />
      )}
    </View>
  );
}

// import React from 'react';

// import DateTimePicker from '@react-native-community/datetimepicker';

// export default function DatePicketInput({data, setData}) {
//   return <DateTimePicker />;
// }
