import React from 'react';
import {
  Collapse,
  CollapseHeader,
  CollapseBody,
} from 'accordion-collapse-react-native';

import {CaixaItemCollapse, ImagemItem, TextItem, InputItem} from './styles';

export default function CollapseMaterial({
  imagem,
  texto,
  styleText,
  styleImagem,
}) {
  return (
    <Collapse>
      <CollapseHeader>
        <CaixaItemCollapse>
          <ImagemItem style={styleImagem} source={imagem} />
          <TextItem style={styleText}>{texto}</TextItem>
        </CaixaItemCollapse>
      </CollapseHeader>
      <CollapseBody
        style={{width: '100%', paddingLeft: 120, paddingRight: 120}}>
        <CaixaItemCollapse>
          <InputItem keyboardType="numeric" placeholder="Peso" />
        </CaixaItemCollapse>
      </CollapseBody>
    </Collapse>
  );
}
