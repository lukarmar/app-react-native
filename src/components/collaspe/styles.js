import styled from 'styled-components/native';

export const CaixaItemCollapse = styled.View`
  flex-direction: row-reverse;
  align-items: center;
  justify-content: center;
`;

export const ImagemItem = styled.Image`
  left: 165px;
  width: 18px;
`;

export const TextItem = styled.Text`
  font-size: 18px;
  color: #000;
  font-weight: 600;
  margin-bottom: 10px;
`;

export const InputItem = styled.TextInput`
  width: 100%;
  padding-bottom: 6px;
  height: 30px;
  border: 1px solid #000;
`;
