import styled from 'styled-components/native';

export const Container = styled.View`
  width: 100%;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  height: auto;
`;
export const Img = styled.Image`
  width: 32px;
  height: 32px;
  margin-right: 5px;
`;

export const TextoLabelData = styled.Text`
  color: #000;
  margin-left: 4px;
  margin-bottom: 2px;
  font-size: 16px;
`;
