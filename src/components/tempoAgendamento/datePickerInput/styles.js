import styled from 'styled-components/native';
import {CheckBox} from 'react-native-elements';

export const Container = styled.View`
  width: 100%;
  height: 320px;
  background: #fff;

  border-radius: 4px;
  padding: 26px 22px 5px 22px;

  justify-content: space-between;
  align-items: flex-start;
`;
export const Titulo = styled.Text`
  height: auto;
  margin-bottom: 50px;
  font-size: 22px;
  text-align: center;
  color: #000;
  letter-spacing: 1px;
  font-weight: bold;
`;

export const CheckBoxSexo = styled(CheckBox)``;

export const TextBotaoOk = styled.Text`
  font-size: 14px;
  color: #fff;
`;

export const BotaoOk = styled.TouchableOpacity`
  width: 50px;
  padding: 4px;
  background: #ff7200;
  border-radius: 4px;
  justify-content: center;
  align-items: center;
`;

export const TextBotaoCancel = styled.Text`
  font-size: 14px;
  color: #000;
`;

export const BotaoCancel = styled.TouchableOpacity`
  width: auto;
  margin-right: 20px;
  padding: 4px;
  background: transparent;
  border-radius: 4px;
  justify-content: center;
  align-items: center;
`;
