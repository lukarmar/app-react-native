import React, {useState, useEffect} from 'react';
import {StyleSheet, View} from 'react-native';
import {format} from 'date-fns';
import pt from 'date-fns/locale/pt';

import DataPicket from './datePickerInput';

import {Container, TextoLabelData, Img} from './styles';

export default function DatePicker({mode, imagem}) {
  const [show, setShow] = useState(false);
  const [date, setDate] = useState(new Date());
  const [dataFormatada, setDataFormatada] = useState('');

  useEffect(() => {
    function formatarData(data) {
      let formatacao = '';
      if (mode === 'date') {
        formatacao = format(data, "dd'/'MM'/'yyyy", {locale: pt});
      }
      if (mode === 'time') {
        formatacao = format(data, "HH':'mm", {locale: pt});
      }

      setDataFormatada(formatacao);
    }
    formatarData(date);
  }, [date, mode]);

  return (
    <>
      <Container>
        <Img source={imagem} />
        <TextoLabelData onPress={() => setShow(!show)}>
          {dataFormatada}
        </TextoLabelData>
      </Container>
      <DataPicket
        show={show}
        setShow={setShow}
        date={date}
        setDate={setDate}
        mode={mode}
      />
    </>
  );
}
