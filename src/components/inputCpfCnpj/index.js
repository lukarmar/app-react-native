import React, {useState} from 'react';
import {
  Container,
  TextoLabel,
  CaixaBotao,
  BotaoCpf,
  TextoBotaoCpf,
  BotaoCnpj,
  TextoBotaoCnpj,
} from './styles';

import Modal from './modal';

export default function InputCpfCnpj() {
  const [visivel, setVisivel] = useState(false);
  const [mascara, setMascara] = useState('cpf');
  const [respostaCadatsro, setRespostaCadatsro] = useState();

  function retornaDado() {
    if (respostaCadatsro) {
      return respostaCadatsro;
    }
    return mascara === 'cpf' ? '000.000.000-00' : '00.000.000/0000-00';
  }

  function mantemDado() {
    if (respostaCadatsro) {
      return;
    }
    setRespostaCadatsro();
  }

  return (
    <Container>
      <CaixaBotao>
        <BotaoCpf mascara={mascara} onPress={() => setMascara('cpf')}>
          <TextoBotaoCpf mascara={mascara}>CPf</TextoBotaoCpf>
        </BotaoCpf>
        <BotaoCnpj mascara={mascara} onPress={() => setMascara('cnpj')}>
          <TextoBotaoCnpj mascara={mascara}>CNPJ</TextoBotaoCnpj>
        </BotaoCnpj>
      </CaixaBotao>
      <TextoLabel
        onPress={() => {
          setVisivel(!visivel);
          mantemDado();
        }}>
        {retornaDado()}
      </TextoLabel>
      <Modal
        visivel={visivel}
        setVisivel={setVisivel}
        mascara={mascara}
        setMascara={setMascara}
        respostaCadatsro={respostaCadatsro}
        setRespostaCadatsro={setRespostaCadatsro}
      />
    </Container>
  );
}
