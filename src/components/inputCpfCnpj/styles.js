import styled from 'styled-components/native';

export const Container = styled.View`
  width: 100%;
  margin-top: 25px;
`;
export const TextoLabel = styled.Text`
  border-bottom-width: 1px;
  border-bottom-color: #8b8b8b;
  padding: 0 0 6px 2px;
  color: #000a;
  font-size: 17px;
`;
export const CaixaBotao = styled.View`
  flex-direction: row;
  justify-content: center;
  margin-bottom: 25px;
`;

export const BotaoCpf = styled.TouchableOpacity`
  width: 102px;
  height: 26px;
  border: 1px solid #000;
  border-right-width: 0;
  background: ${props => (props.mascara === 'cpf' ? '#000' : '#fff')};

  justify-content: center;
  align-items: center;
`;
export const TextoBotaoCpf = styled.Text`
  font-size: 15px;
  color: ${props => (props.mascara === 'cpf' ? '#fff' : '#000')};
`;

export const BotaoCnpj = styled.TouchableOpacity`
  width: 102px;
  height: 26px;
  border: 1px solid #000;
  border-left-width: 0;
  background: ${props => (props.mascara === 'cnpj' ? '#000' : '#fff')};

  justify-content: center;
  align-items: center;
`;
export const TextoBotaoCnpj = styled.Text`
  font-size: 15px;
  color: ${props => (props.mascara === 'cnpj' ? '#fff' : '#000')};
`;
