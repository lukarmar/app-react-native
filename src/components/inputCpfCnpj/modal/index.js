import React, {useEffect, useRef, useState} from 'react';
import {View, Keyboard} from 'react-native';
import ReactModal from 'react-native-modal';

import {
  Container,
  CaixaBotao,
  BotaoCpf,
  TextoBotaoCpf,
  BotaoCnpj,
  TextoBotaoCnpj,
  Input,
  TextoErro,
  BotaoSalvar,
  TextBotaoSalvar,
} from './styles';

export default function Modal({
  visivel,
  setVisivel,
  mascara,
  setMascara,
  respostaCadatsro,
  setRespostaCadatsro,
}) {
  const refInput = useRef(null);
  const [erro, setErro] = useState();
  const [mascaraModal, setMascaraModal] = useState(mascara);

  useEffect(() => {
    setRespostaCadatsro('');
    setErro();
  }, [mascaraModal, setRespostaCadatsro]);

  useEffect(() => {
    setMascaraModal(mascara);
  }, [mascara]);

  function salvar() {
    if (!refInput.current.isValid()) {
      setErro(`${refInput.current.props.type} inválido`);
      return;
    }
    Keyboard.dismiss();
    setErro();
    setTimeout(() => {
      setVisivel(!visivel);
    }, 300);
  }
  return (
    <ReactModal
      animationOutTiming={900}
      backdropTransitionOutTiming={0}
      isVisible={visivel}
      onBackdropPress={() => {
        setRespostaCadatsro('');
        setVisivel(!visivel);
        setMascara(mascaraModal);
        setErro();
      }}>
      <Container>
        <CaixaBotao>
          <BotaoCpf
            mascara={mascaraModal}
            onPress={() => setMascaraModal('cpf')}>
            <TextoBotaoCpf
              mascara={mascaraModal}
              onPress={() => setMascaraModal('cpf')}>
              CPF
            </TextoBotaoCpf>
          </BotaoCpf>
          <BotaoCnpj
            mascara={mascaraModal}
            onPress={() => setMascaraModal('cnpj')}>
            <TextoBotaoCnpj
              mascara={mascaraModal}
              onPress={() => setMascaraModal('cnpj')}>
              CNPJ
            </TextoBotaoCnpj>
          </BotaoCnpj>
        </CaixaBotao>
        <Input
          ref={refInput}
          type={mascaraModal}
          value={respostaCadatsro}
          onChangeText={text => {
            setRespostaCadatsro(text);
          }}
        />
        <TextoErro>{erro}</TextoErro>
        <View style={{width: '100%', paddingRight: 80, paddingLeft: 80}}>
          <BotaoSalvar onPress={() => salvar()}>
            <TextBotaoSalvar>salvar</TextBotaoSalvar>
          </BotaoSalvar>
        </View>
      </Container>
    </ReactModal>
  );
}
