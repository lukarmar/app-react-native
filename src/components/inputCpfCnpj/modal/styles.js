import styled from 'styled-components/native';
import {TextInputMask} from 'react-native-masked-text';

export const Container = styled.View`
  width: 100%;
  height: 250px;
  background: #fff;

  border-radius: 4px;
  padding: 26px 22px 5px 22px;

  justify-content: space-between;
  align-items: center;
`;

export const CaixaBotao = styled.View`
  flex-direction: row;
  justify-content: center;
  margin-bottom: 25px;
`;

export const BotaoCpf = styled.TouchableOpacity`
  width: 102px;
  height: 26px;
  border: 1px solid #000;
  border-right-width: 0;
  background: ${props => (props.mascara === 'cpf' ? '#000' : '#fff')};

  justify-content: center;
  align-items: center;
`;
export const TextoBotaoCpf = styled.Text`
  font-size: 15px;
  color: ${props => (props.mascara === 'cpf' ? '#fff' : '#000')};
`;

export const BotaoCnpj = styled.TouchableOpacity`
  width: 102px;
  height: 26px;
  border: 1px solid #000;
  border-left-width: 0;
  background: ${props => (props.mascara === 'cnpj' ? '#000' : '#fff')};

  justify-content: center;
  align-items: center;
`;
export const TextoBotaoCnpj = styled.Text`
  font-size: 15px;
  color: ${props => (props.mascara === 'cnpj' ? '#fff' : '#000')};
`;

export const Input = styled(TextInputMask)`
  width: 100%;
  padding-bottom: 0;
  border-bottom-width: 1px;
  border-bottom-color: #ff7200;
`;

export const TextoErro = styled.Text`
  color: #ff7200;
  font-size: 14px;

  margin-top: -10px;
  font-weight: bold;
`;

export const BotaoSalvar = styled.TouchableOpacity`
  width: 100%;
  padding: 8px;
  background: #ff7200;
  border-radius: 4px;
  margin: 45px 0 5px 0;
  justify-content: center;
  align-items: center;
`;

export const TextBotaoSalvar = styled.Text`
  font-size: 14px;
  color: #fff;
`;
