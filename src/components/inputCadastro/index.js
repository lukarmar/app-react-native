import React, {useState} from 'react';
import {View, StyleSheet} from 'react-native';

import Modal from '../inputCadastro/modal';

import {Container, TextoLabel, Labeltop, TextoLabelSexo} from './styles';

export default function InputCadastro() {
  const [visivel, setVisivel] = useState(false);
  const [respostaSexo, setRespostaSexo] = useState('');
  const [sexo, setSexo] = useState({});

  function retornaSexo() {
    for (var label in sexo) {
      sexo[label] && setRespostaSexo(label);
    }
  }

  const style = StyleSheet.create({
    labelTop: {
      translateY: -10,
    },
  });

  return (
    <Container>
      {respostaSexo ? (
        <View style={style.labelTop}>
          <Labeltop>Sexo</Labeltop>
        </View>
      ) : (
        <></>
      )}

      {respostaSexo ? (
        <TextoLabelSexo
          onPress={() => {
            setVisivel(!visivel);
          }}>
          {respostaSexo}
        </TextoLabelSexo>
      ) : (
        <TextoLabel
          onPress={() => {
            setVisivel(!visivel);
          }}>
          Sexo
        </TextoLabel>
      )}
      <Modal
        visivel={visivel}
        setVisivel={setVisivel}
        sexo={sexo}
        setSexo={setSexo}
        retornaSexo={retornaSexo}
      />
    </Container>
  );
}
