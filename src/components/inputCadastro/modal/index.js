import React, {useState} from 'react';
import {View} from 'react-native';
import ReactModal from 'react-native-modal';

import {
  Container,
  Titulo,
  CheckBoxSexo,
  TextBotaoOk,
  BotaoOk,
  BotaoCancel,
  TextBotaoCancel,
} from './styles';

export default function Modal({
  visivel,
  setVisivel,
  sexo,
  setSexo,
  retornaSexo,
}) {
  function cancelar() {
    setSexo({masculino: false, feminino: false, outro: false});
    setVisivel(false);
  }
  function empurraDadoSexo() {
    retornaSexo();
    setVisivel(false);
  }
  return (
    <ReactModal
      animationOutTiming={400}
      backdropTransitionOutTiming={0}
      isVisible={visivel}
      onBackdropPress={() => setVisivel(!visivel)}>
      <Container>
        <Titulo>Sexo</Titulo>
        <View style={{flex: 1}}>
          <CheckBoxSexo
            onPress={() => {
              setSexo({Masculino: true, Feminino: false, Outro: false});
            }}
            title="Masculino"
            checkedIcon="dot-circle-o"
            uncheckedIcon="circle-o"
            checked={sexo.Masculino}
            textStyle={{color: '#000000', fontSize: 18}}
            containerStyle={[
              {
                backgroundColor: '#fff',
                borderColor: '#fff',
                padding: 0,
                margin: 0,
                marginLeft: 0,
              },
            ]}
          />
          <CheckBoxSexo
            onPress={() => {
              setSexo({Masculino: false, Feminino: true, Outro: false});
            }}
            title="Feminino"
            checkedIcon="dot-circle-o"
            uncheckedIcon="circle-o"
            checked={sexo.Feminino}
            textStyle={{color: '#000000', fontSize: 18}}
            containerStyle={[
              {
                backgroundColor: '#fff',
                borderColor: '#fff',
                padding: 0,
                marginLeft: 0,
              },
            ]}
          />
          <CheckBoxSexo
            onPress={() => {
              setSexo({Masculino: false, Feminino: false, Outro: true});
            }}
            title="Outro"
            checkedIcon="dot-circle-o"
            uncheckedIcon="circle-o"
            checked={sexo.Outro}
            textStyle={{color: '#000000', fontSize: 18}}
            containerStyle={[
              {
                backgroundColor: '#fff',
                borderColor: '#fff',
                padding: 0,
                margin: 0,
                marginLeft: 0,
              },
            ]}
          />
        </View>
        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            justifyContent: 'flex-end',
          }}>
          <BotaoCancel>
            <TextBotaoCancel onPress={() => cancelar()}>
              Cancelar
            </TextBotaoCancel>
          </BotaoCancel>
          <BotaoOk onPress={() => empurraDadoSexo()}>
            <TextBotaoOk>OK</TextBotaoOk>
          </BotaoOk>
        </View>
      </Container>
    </ReactModal>
  );
}
