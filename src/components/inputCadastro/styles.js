import styled from 'styled-components/native';

export const Container = styled.View`
  width: 100%;
  height: auto;
  /* margin-top: 10px; */
  border-bottom-width: 0.4px;
  border-bottom-color: #8b8b8b;
  padding: 10px 0 6px 2px;
`;
export const TextoLabel = styled.Text`
  color: #000a;
  margin: 17px 0 1px 1px;
  font-size: 16.9px;
`;

// export const Container = styled.View`
//   width: 100%;
//   height: auto;
//   /* margin-top: 20px; */
//   border-bottom-width: 1px;
//   border-bottom-color: #8b8b8b;
//   padding: 0 0 6px 2px;
// `;
// export const TextoLabelPlaceholder = styled.Text`
//   color: #0003;
//   margin: 17px 0 1px 1px;
//   font-size: 16.7px;
// `;

export const TextoLabelSexo = styled.Text`
  color: #000;
  margin-left: 4px;
  margin-bottom: 2px;
  font-size: 16.7px;
`;

export const Labeltop = styled.Text`
  color: #8b8b8b;
  font-size: 16px;
  margin-top: 6px;
`;
