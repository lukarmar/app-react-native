import styled from 'styled-components/native';

export const Container = styled.TouchableOpacity`
  width: 100%;
  height: 58px;
  background: #ff7200;
  border-radius: 4px;

  align-items: center;
  justify-content: center;
`;
export const Text = styled.Text`
  font-size: 20px;
  font-weight: 600;
  color: #fff;
`;
