import React from 'react';
import {ScrollView} from 'react-native';

import {imagens} from '../../../util/enderecosImagens';

import {Container, Titulo, Termo, ImagemRecycle} from './styles';

export default function PociticaPrivacidade() {
  return (
    <ScrollView keyboardShouldPersistTaps="handled">
      <Container>
        <Titulo>Política de privacidade para recycle din </Titulo>
        <Termo>
          Todas as suas informações pessoais recolhidas, serão usadas para o
          ajudar a tornar a sua visita no nosso site o mais produtiva e
          agradável possível.
          {'\n'}
          {'\n'}A garantia da confidencialidade dos dados pessoais dos
          utilizadores do nosso aplicativo é importante para o recycle din.
          {'\n'}
          {'\n'}
          Todas as informações pessoais relativas a membros, assinantes,
          clientes ou visitantes que usem o recycle din serão tratadas em
          concordância com a Lei da Proteção de Dados Pessoais de 26 de outubro
          de 1998 (Lei n.º 67/98).
          {'\n'}
          {'\n'}A informação pessoal recolhida pode incluir o seu nome, e-mail,
          número de telefone e/ou telemóvel, morada, data de nascimento e/ou
          outros.
          {'\n'}
          {'\n'}O uso do recycle din pressupõe a aceitação deste Acordo de
          privacidade. A equipa do recycle din reserva-se ao direito de alterar
          este acordo sem aviso prévio. Deste modo, recomendamos que consulte a
          nossa política de privacidade com regularidade de forma a estar sempre
          atualizado.
        </Termo>
        <ImagemRecycle source={imagens.RecycleDinnome} />
      </Container>
    </ScrollView>
  );
}
