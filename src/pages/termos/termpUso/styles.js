import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  align-items: center;
  background: #fff;
  padding: 34px 15px 15px 10px;
`;
export const Titulo = styled.Text`
  font-size: 15px;
  color: #ff7200;
  font-weight: 600;
`;
export const Termo = styled.Text`
  font-size: 15px;
  margin: 18px 0 90px;
  color: #000;
  font-weight: 600;
  text-align: left;
`;

export const ImagemRecycle = styled.Image`
  width: 161px;
  height: 50px;
`;
