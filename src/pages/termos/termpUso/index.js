import React from 'react';
import {ScrollView} from 'react-native';

import {imagens} from '../../../util/enderecosImagens';

import {Container, Titulo, Termo, ImagemRecycle} from './styles';

export default function EntrarContato() {
  return (
    <ScrollView keyboardShouldPersistTaps="handled">
      <Container>
        <Titulo>Termos de uso</Titulo>
        <Termo>
          Os serviços disponibilizados no aplicativo Recycle Din apenas poderão
          ser acessados por pessoas plenamente capazes, conforme o Direito
          brasileiro. Todos aqueles que não possuírem plena capacidade civil -
          menores de 18 anos não emancipados, pródigos, ébrios habituais ou
          viciados em tóxicos e pessoas que não puderem exprimir sua vontade,
          por motivo transitório ou permanente - deverão ser devidamente
          assistidos por seus representantes legais, que se responsabilizarão
          pelo cumprimento das presentes regras.
          {'\n'}
          {'\n'}Ao usuário, será permitido manter apenas uma conta junto ao
          aplicativo Recycle Din. Contas duplicadas serão automaticamente
          desativadas pelo editor do aplicativo, sem prejuízo de demais
          penalidades cabíveis.
          {'\n'}
          {'\n'}Para o devido cadastramento junto ao serviço, o usuário deverá
          fornecer integralmente os dados requeridos. Todas as informações
          fornecidas pelo usuário devem ser precisas, verdadeiras e atualizadas.
          Em qualquer caso, o usuário responderá, em âmbito cível e criminal,
          pela veracidade, exatidão e autenticidade dos dados informados.
        </Termo>
        <ImagemRecycle source={imagens.RecycleDinnome} />
      </Container>
    </ScrollView>
  );
}
