import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  background: #fff;
`;

export const Banner = styled.ImageBackground`
  height: 410px;
  align-items: center;
  justify-content: center;
`;
export const Logo = styled.Image`
  width: 253px;
  height: 253px;
`;
export const TaxtoBanner = styled.Text`
  font-size: 19px;
  font-weight: 600;
  color: #fff;
  letter-spacing: 1px;

  margin: 32px 0 0 0;
`;

export const CaixaAutentic = styled.View`
  padding: 30px 6px 06px;
`;

export const BotaoAut = styled.TouchableOpacity`
  width: 100%;
  height: 58px;
  padding: 13px 50px 13px 0;
  border: 1px #000 solid;
  margin-bottom: 12px;

  flex-direction: row;
  justify-content: center;
  align-items: center;
`;
export const IconeFace = styled.Image`
  width: 58px;
  height: 58px;
  margin: 0 20px 0 0;
`;
export const IconeGoogle = styled.Image`
  width: 40px;
  height: 40px;
  margin: 0 29px 0 -15px;
`;
export const IconeEmal = styled.Image`
  width: 38px;
  height: 38px;
  margin: 0 30px 0 -5px;
`;
export const TextoBotao = styled.Text`
  font-size: 20px;
  color: #000;
  font-weight: 600;
`;

export const Traco = styled.View`
  border-top-width: 1px;
  border-top-color: #8b8b8b;
  margin-top: 22px;
`;

export const TextoMembro = styled.Text`
  width: 115px;
  background: #fff;
  font-size: 15px;
  color: #8b8b8b;
  text-align: center;

  align-self: center;
`;
