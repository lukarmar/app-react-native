import React, {useState} from 'react';
import {StyleSheet, ScrollView} from 'react-native';

import {imagens} from '../../util/enderecosImagens';
import BotaoLogin from '../../components/Button';
import Modal from './modal';

import {
  Container,
  Banner,
  Logo,
  TaxtoBanner,
  CaixaAutentic,
  BotaoAut,
  IconeFace,
  IconeGoogle,
  IconeEmal,
  TextoBotao,
  Traco,
  TextoMembro,
} from './styles';

const style = StyleSheet.create({
  posicao: {
    transform: [
      {
        translateY: -12,
      },
    ],
  },
});

export default function Autenticacao() {
  const [modalVisivel, setModalVisivel] = useState(false);
  return (
    <ScrollView keyboardShouldPersistTaps="handled">
      <Container>
        <Banner source={imagens.banner}>
          <Logo source={imagens.logo} />
          <TaxtoBanner>TRANSFORMANDO LIXO EM RIQUEZA</TaxtoBanner>
        </Banner>
        <CaixaAutentic>
          <BotaoAut onPress={() => setModalVisivel(!modalVisivel)}>
            <IconeFace source={imagens.iconeFacebook} />
            <TextoBotao>Continuar com o Facebook</TextoBotao>
          </BotaoAut>
          <BotaoAut onPress={() => setModalVisivel(!modalVisivel)}>
            <IconeGoogle source={imagens.iconeGoogle} />
            <TextoBotao>Continuar com o Google</TextoBotao>
          </BotaoAut>
          <BotaoAut onPress={() => setModalVisivel(!modalVisivel)}>
            <IconeEmal source={imagens.iconeEmail} />
            <TextoBotao>Inscreva-se com o E-mail</TextoBotao>
          </BotaoAut>
          <Traco />
          <TextoMembro style={style.posicao}>Já sou membro</TextoMembro>
          <BotaoLogin>Login</BotaoLogin>
        </CaixaAutentic>
      </Container>
      <Modal visivel={modalVisivel} setVisivel={setModalVisivel} />
    </ScrollView>
  );
}
