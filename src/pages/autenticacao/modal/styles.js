import styled from 'styled-components/native';

export const Container = styled.View`
  width: 100%;
  height: 320px;
  background: #fff;

  border-radius: 4px;
  padding: 26px 22px 41px 22px;

  justify-content: space-between;
  align-items: center;
`;
export const Titulo = styled.Text`
  font-size: 22px;
  text-align: center;
  color: #000;
  letter-spacing: 1px;
  font-weight: bold;
`;
export const Aviso = styled.Text`
  font-size: 20px;
  text-align: left;
  color: #000;
`;

export const TextoTermos = styled.Text`
  text-decoration: underline;
`;
export const TextoPolitica = styled.Text`
  text-decoration: underline;
`;

export const Botao = styled.TouchableOpacity`
  width: 100%;
  height: 58px;
  background: #ff7200;
  border-radius: 4px;

  align-items: center;
  justify-content: center;
`;
export const TextBotao = styled.Text`
  font-size: 20px;
  font-weight: 600;
  color: #fff;
`;
