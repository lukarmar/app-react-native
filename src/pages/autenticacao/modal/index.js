import React from 'react';
import ReactModal from 'react-native-modal';
import {useNavigation} from '@react-navigation/native';
import {Text} from 'react-native';

import {
  Container,
  Titulo,
  Aviso,
  Botao,
  TextBotao,
  TextoTermos,
  TextoPolitica,
} from './styles';

export default function Modal({visivel, setVisivel}) {
  const navigation = useNavigation();

  function navegacaoVisilidadeTermo() {
    setVisivel(!visivel);
    navigation.navigate('TermoUso');
  }

  function navegacaoVisilidadePolitica() {
    setVisivel(!visivel);
    navigation.navigate('PoliticaPrivacidade');
  }

  function navegacaoVisilidadeConcordar() {
    setVisivel(!visivel);
    navigation.navigate('CadastroFornecedor');
  }

  return (
    <ReactModal
      animationOutTiming={400}
      backdropTransitionOutTiming={0}
      isVisible={visivel}
      onBackdropPress={() => setVisivel(!visivel)}>
      <Container>
        <Titulo>Concorde com os nossos termos</Titulo>
        <Aviso>
          Ao se inscrever, você concorda com nossos{' '}
          <TextoTermos onPress={() => navegacaoVisilidadeTermo()}>
            Termos de serviço.
          </TextoTermos>{' '}
          Consulte nossa{' '}
          <TextoPolitica onPress={() => navegacaoVisilidadePolitica()}>
            Política de privacidade.
          </TextoPolitica>
        </Aviso>
        <Botao onPress={() => navegacaoVisilidadeConcordar()}>
          <TextBotao>Concordar e inscrever-se</TextBotao>
        </Botao>
      </Container>
    </ReactModal>
  );
}
