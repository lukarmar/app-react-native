import React from 'react';
import {ScrollView} from 'react-native';
import {useNavigation} from '@react-navigation/native';

import {imagens} from '../../../util/enderecosImagens';

import {
  Container,
  ImagemCaixas,
  CaixaConteudos,
  Titulo,
  Termo,
  CaixaBotoes,
  BotaoSim,
  LinkNao,
} from './styles';

export default function ProntoComecar() {
  const navigation = useNavigation();

  return (
    <ScrollView keyboardShouldPersistTaps="handled">
      <Container>
        <ImagemCaixas source={imagens.imgProntoComecar} />
        <CaixaConteudos>
          <Titulo>Pronto para começar?</Titulo>
          <Termo>
            O melhor jeito de experimentar o Recycle Din é Reciclando. {'\n'}
            {'\n'}
            Comece agora a ver as nossas dicas e o que podemos fazer para deixar
            um mundo melhor para nossos filhos e netos. {'\n'}
            {'\n'}Vamos mudar nossa cultura e a forma como nos relacionamento
            com nosso lixo?
          </Termo>
          <CaixaBotoes>
            <BotaoSim onPress={() => navigation.navigate('PermitirAcesso')}>
              Vamos lá
            </BotaoSim>
            <LinkNao>Mais tarde</LinkNao>
          </CaixaBotoes>
        </CaixaConteudos>
      </Container>
    </ScrollView>
  );
}
