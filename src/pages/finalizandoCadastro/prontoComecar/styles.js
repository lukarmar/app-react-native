import styled from 'styled-components/native';
import Botao from '../../../components/Button';

export const Container = styled.View`
  flex: 1;
  align-items: center;
  background: #fff;
`;
export const ImagemCaixas = styled.Image`
  width: 100%;
  height: 236px;
`;

export const CaixaConteudos = styled.View`
  width: 100%;
  padding: 16.5px 10px 25px 10px;
  align-items: center;
`;

export const Titulo = styled.Text`
  font-size: 25px;
  color: #000000;
  letter-spacing: 1.8px;
  text-align: center;
  font-weight: bold;
  margin-bottom: 16px;
`;

export const Termo = styled.Text`
  font-size: 18px;
  margin: 10px 0 70px;
  letter-spacing: 0.5px;
  color: #929292;
  font-weight: 600;
  text-align: left;
`;

export const CaixaBotoes = styled.View`
  width: 100%;
  padding: 0 60px;
  align-items: center;
`;

export const BotaoSim = styled(Botao)`
  height: 40px;
  margin-bottom: 10px;
`;

export const LinkNao = styled.Text`
  font-size: 20px;
  color: #000;
  text-decoration: underline;
`;
