import React from 'react';
import {ScrollView} from 'react-native';
import {useNavigation} from '@react-navigation/native';

import {imagens} from '../../../util/enderecosImagens';

import {
  Container,
  ImagemCaixas,
  CaixaConteudos,
  Titulo,
  Termo,
  CaixaBotoes,
  BotaoSim,
  LinkNao,
} from './styles';

export default function NormasComunidade() {
  const navigation = useNavigation();

  return (
    <ScrollView keyboardShouldPersistTaps="handled">
      <Container>
        <ImagemCaixas source={imagens.imgNormasComu} />
        <CaixaConteudos>
          <Titulo>Normas da Comunidade do Recycle Din</Titulo>
          <Termo>
            Seja um embaixador da natureza sendo cauteloso, respeitando a sua
            segurança e das outras pessoas e obedecendo às leis. O objetivo da
            nossa comunidade é criar um mundo melhor para nossos filhos, netos e
            gerações posteriores. Vamos incentivar de forma responsável o
            respeito ao meio ambiente e mostrar que todos dependem dele.
          </Termo>
          <CaixaBotoes>
            <BotaoSim onPress={() => navigation.navigate('ProntoComecar')}>
              CONTINUAR
            </BotaoSim>
          </CaixaBotoes>
        </CaixaConteudos>
      </Container>
    </ScrollView>
  );
}
