import React from 'react';
import {ScrollView} from 'react-native';
import {useNavigation} from '@react-navigation/native';

import {imagens} from '../../../util/enderecosImagens';

import {
  Container,
  ImagemCaixas,
  CaixaConteudos,
  Titulo,
  Termo,
  CaixaBotoes,
  BotaoSim,
  LinkNao,
} from './styles';

export default function TermosUso() {
  const navigation = useNavigation();

  return (
    <ScrollView keyboardShouldPersistTaps="handled">
      <Container>
        <ImagemCaixas source={imagens.imgEntrarContato} />
        <CaixaConteudos>
          <Titulo>Podemos entrar em contato com você?</Titulo>
          <Termo>
            Gostaríamos de enviar as seguintes informações por e-mail para você:
            {'\n'}- Suas estatísticas mensais; {'\n'}- Dicas de como aproveitar
            o Recycle Din;{'\n'}- Dicas de Reciclagem e Reaproveitamento;{'\n'}-
            Informações atualizadas sobre os recursos, novidades e desafios;{' '}
            {'\n'}- Histórias da comunidade do Recycle Din;
          </Termo>
          <CaixaBotoes>
            <BotaoSim onPress={() => navigation.navigate('NormasComunidade')}>
              SIM
            </BotaoSim>
            <LinkNao>Não</LinkNao>
          </CaixaBotoes>
        </CaixaConteudos>
      </Container>
    </ScrollView>
  );
}
