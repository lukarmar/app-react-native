import React, {useState} from 'react';
import {ScrollView} from 'react-native';
import {useNavigation} from '@react-navigation/native';

import Modal from './modal';

import {imagens} from '../../../util/enderecosImagens';

import {
  Container,
  ImagemCaixas,
  CaixaConteudos,
  Titulo,
  Termo,
  CaixaBotoes,
  BotaoSim,
  LinkMaisTarde,
} from './styles';

export default function PermitirAcesso() {
  const [modalVisivel, setModalVisivel] = useState(false);
  const navigation = useNavigation();

  return (
    <ScrollView keyboardShouldPersistTaps="handled">
      <Container>
        <ImagemCaixas source={imagens.imgPermitirAcesso} />
        <CaixaConteudos>
          <Titulo>Permitir acesso à localização do dispositivo</Titulo>
          <Termo>
            Comece a reciclar e veja sugestões na sua região compartilhando a
            localização do seu dispositivo com o Recycle Din
          </Termo>
          <CaixaBotoes>
            <BotaoSim onPress={() => setModalVisivel(!modalVisivel)}>
              Sim
            </BotaoSim>
            <LinkMaisTarde>Mais tarde</LinkMaisTarde>
          </CaixaBotoes>
        </CaixaConteudos>
      </Container>
      <Modal visivel={modalVisivel} setVisivel={setModalVisivel} />
    </ScrollView>
  );
}
