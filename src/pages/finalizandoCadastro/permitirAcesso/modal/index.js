import React from 'react';
import ReactModal from 'react-native-modal';
import {useNavigation} from '@react-navigation/native';
import {Text} from 'react-native';

import {
  Container,
  Titulo,
  Aviso,
  Botao,
  TextBotao,
  CaixaBotoes,
  BotaoConfig,
  BotaoCancel,
} from './styles';

export default function Modal({visivel, setVisivel}) {
  const navigation = useNavigation();

  function navegacaoVisilidadeMapa() {
    setVisivel(!visivel);
    navigation.navigate('MapaInicial');
  }

  return (
    <ReactModal
      animationOutTiming={400}
      backdropTransitionOutTiming={0}
      isVisible={visivel}
      onBackdropPress={() => setVisivel(!visivel)}>
      <Container>
        <Titulo>Modo de Economia de bateria</Titulo>
        <Aviso>
          Para registrar com precisção sua localização com o GPS, é necessário
          desativar o modo de economia de bateria.
        </Aviso>
        <CaixaBotoes>
          <BotaoCancel onPress={() => navegacaoVisilidadeMapa()}>
            <TextBotao>cancelar</TextBotao>
          </BotaoCancel>
          <BotaoConfig>
            <TextBotao>configurações</TextBotao>
          </BotaoConfig>
        </CaixaBotoes>
      </Container>
    </ReactModal>
  );
}
