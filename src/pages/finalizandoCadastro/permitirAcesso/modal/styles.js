import styled from 'styled-components/native';
import Botao from '../../../../components/Button';

export const Container = styled.View`
  width: 100%;
  height: 320px;
  background: #fff;

  border-radius: 4px;
  padding: 26px 22px 20px 22px;

  justify-content: space-between;
  align-items: center;
`;
export const Titulo = styled.Text`
  font-size: 22px;
  text-align: center;
  color: #000;
  letter-spacing: 1px;
  font-weight: bold;
`;
export const Aviso = styled.Text`
  width: 100%;
  font-size: 18px;
  text-align: left;
  color: #929292;
  margin-bottom: 50px;
`;

export const TextBotao = styled.Text`
  width: 100%;
  color: #fff;
  font-size: 15px;
`;

export const CaixaBotoes = styled.View`
  justify-content: center;
  flex-direction: row;
  height: auto;
`;

export const BotaoConfig = styled(Botao)`
  background: #ff7200;
  margin-left: 16px;
  flex: 1;
  height: 28px;
  justify-content: center;
  align-items: center;
`;
export const BotaoCancel = styled(Botao)`
  background: #929292;
  flex: 1;
  height: 28px;
  justify-content: center;
  align-items: center;
`;
