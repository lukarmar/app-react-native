import styled from 'styled-components/native';
import Botao from '../../components/Button';

export const Container = styled.View`
  flex: 1;
  background: #fff;
`;

export const Banner = styled.ImageBackground`
  height: 410px;
  align-items: center;
  justify-content: center;
`;
export const Logo = styled.Image`
  width: 253px;
  height: 253px;
`;
export const TaxtoBanner = styled.Text`
  font-size: 19px;
  font-weight: 600;
  color: #fff;
  letter-spacing: 1px;

  margin: 32px 0 0 0;
`;

export const CaixaAutentic = styled.View`
  justify-content: center;
  align-items: center;
  padding: 30px 6px 06px;
`;
export const TextoEscolha = styled.Text`
  font-size: 20px;
  text-decoration: underline;
  margin-bottom: 30px;
`;

export const BotaoFornecedor = styled.TouchableOpacity`
  width: 100%;
  height: 58px;
  background: ${props => (props.estiloFornecedor ? '#000' : '#fff')};
  color: ${props => (props.estiloFornecedor ? '#fff' : '#000')};
  padding: 13px 0;
  border: 1px #000 solid;
  margin-bottom: 22px;
  align-items: center;
`;
export const TextoBotaoFornecedor = styled.Text`
  font-size: 20px;
  color: #000;
  font-weight: 600;
  color: ${props => (props.estiloFornecedor ? '#fff' : '#000')};
`;

export const BotaoColetor = styled.TouchableOpacity`
  width: 100%;
  height: 58px;
  background: ${props => (props.estiloColetor ? '#000' : '#fff')};
  padding: 13px 0;
  border: 1px #000 solid;
  margin-bottom: 22px;
  align-items: center;
`;

export const TextoBotaoColetor = styled.Text`
  font-size: 20px;
  color: #000;
  font-weight: 600;
  color: ${props => (props.estiloColetor ? '#fff' : '#000')};
`;

export const CaixaBotao = styled.View`
  width: 100%;
  padding: 0 44px;
`;

export const BotaoaAvancar = styled(Botao)`
  background: ${props => (props.ativo ? '#ff7200' : '#BFBFBF')};
  margin-top: 15px;
`;
