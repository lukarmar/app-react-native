import React, {useState} from 'react';
import {ScrollView} from 'react-native';
import {useNavigation} from '@react-navigation/native';

import {imagens} from '../../util/enderecosImagens';

import {
  Container,
  Banner,
  Logo,
  TaxtoBanner,
  CaixaAutentic,
  BotaoFornecedor,
  BotaoColetor,
  TextoEscolha,
  TextoBotaoFornecedor,
  TextoBotaoColetor,
  CaixaBotao,
  BotaoaAvancar,
} from './styles';

export default function EscolhaPerfil() {
  const navigation = useNavigation();
  const [visibleUser, setVisibleUser] = useState({
    fornecedor: false,
    coletor: false,
    ativo: false,
  });
  return (
    <ScrollView keyboardShouldPersistTaps="handled">
      <Container>
        <Banner source={imagens.banner}>
          <Logo source={imagens.logo} />
          <TaxtoBanner>TRANSFORMANDO LIXO EM RIQUEZA</TaxtoBanner>
        </Banner>
        <CaixaAutentic>
          <TextoEscolha>Escolha seu perfil</TextoEscolha>
          <BotaoFornecedor
            onPress={() =>
              setVisibleUser({fornecedor: true, coletor: false, ativo: true})
            }
            estiloFornecedor={visibleUser.fornecedor}>
            <TextoBotaoFornecedor estiloFornecedor={visibleUser.fornecedor}>
              Seja um fornecedor
            </TextoBotaoFornecedor>
          </BotaoFornecedor>
          <BotaoColetor
            onPress={() =>
              setVisibleUser({fornecedor: false, coletor: true, ativo: true})
            }
            estiloColetor={visibleUser.coletor}>
            <TextoBotaoColetor estiloColetor={visibleUser.coletor}>
              Seja um coletor
            </TextoBotaoColetor>
          </BotaoColetor>
          <CaixaBotao>
            <BotaoaAvancar
              ativo={visibleUser.ativo}
              onPress={() => navigation.navigate('Autenticacao')}
              disabled={!visibleUser.ativo}>
              Avançar
            </BotaoaAvancar>
          </CaixaBotao>
        </CaixaAutentic>
      </Container>
    </ScrollView>
  );
}
