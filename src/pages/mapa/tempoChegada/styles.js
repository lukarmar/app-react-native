import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  align-items: center;
`;

export const CaixaConteudos = styled.View`
  width: 100%;
  padding: 18px 10px 100px 10px;
  align-items: center;
`;
export const TracoTopo = styled.View`
  width: 100%;
  margin: 10px 0 3px 0;

  border-bottom-width: 3px;
  border-bottom-color: #ff7200;
`;

export const TextoMaterias = styled.Text`
  font-size: 18px;
  font-weight: 600;
`;

export const CaixaBotoesInferInferiores = styled.View`
  width: 100%;
  flex-direction: row;
  padding: 0 75px;
`;

export const BotaoAgora = styled.TouchableOpacity`
  background: #ff7200;
  border-radius: 4px;
  align-items: center;
  padding: 10px 0;
  margin-bottom: 8px;

  width: 100%;
`;
export const TextBotaoAgora = styled.Text`
  font-size: 20px;
  color: #fff;
`;
