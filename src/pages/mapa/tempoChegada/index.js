import React, {useState, useEffect} from 'react';
import {StyleSheet} from 'react-native';
import MapView, {Marker} from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation';
import {useNavigation} from '@react-navigation/native';

import {
  Container,
  TextoMaterias,
  CaixaConteudos,
  TracoTopo,
  CaixaBotoesInferInferiores,
  BotaoAgora,
  TextBotaoAgora,
} from './styles';

export default function TempoChegada() {
  // const [loading, setLoading] = useState(true);
  const [coordinates, setCoordinates] = useState({latitude: 0, longitude: 0});
  const [error, setError] = useState({});
  const navigation = useNavigation();

  useEffect(() => {
    Geolocation.getCurrentPosition(
      ({coords}) => {
        setCoordinates(coords);
        // setLoading(false);
      },
      error => {
        setError({error: error.message});
      },
      {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000},
    );
  }, []);
  const style = StyleSheet.create({
    mapa: {
      width: '100%',
      height: 400,
    },
  });

  function renderPoits() {
    return (
      <Marker
        coordinate={{
          latitude: coordinates.latitude,
          longitude: coordinates.longitude,
        }}
        title="Minha localização"
      />
    );
  }

  return (
    <Container>
      <MapView
        style={style.mapa}
        initialRegion={{
          latitude: coordinates.latitude,
          longitude: coordinates.longitude,
          latitudeDelta: 0.0068,
          longitudeDelta: 0.0068,
        }}>
        {renderPoits()}
      </MapView>

      <CaixaConteudos>
        <TextoMaterias>Catador chega em: 3 min</TextoMaterias>
        <TracoTopo />
      </CaixaConteudos>

      <CaixaBotoesInferInferiores>
        <BotaoAgora onPress={() => navigation.navigate('MapaInicial')}>
          <TextBotaoAgora>Cancelar coleta</TextBotaoAgora>
        </BotaoAgora>
      </CaixaBotoesInferInferiores>
    </Container>
  );
}
