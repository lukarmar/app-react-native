import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  align-items: center;
`;

export const CaixaTopo = styled.View`
  width: 100%;
  flex: 1;
  align-items: center;
  padding: 22px 14.5px 2px 14.5px;

  justify-content: space-between;
`;

export const CaixaConteudos = styled.View`
  width: 100%;
  align-items: center;
`;
export const TracoTopo = styled.View`
  width: 100%;

  margin: 30px 0 3px 0;

  border-bottom-width: 3px;
  border-bottom-color: #ff7200;
`;

export const TextoMaterias = styled.Text`
  font-size: 18px;
  font-weight: bold;
  margin-top: 10px;
`;

export const ConteudoItens = styled.View`
  width: 100%;
  padding: 0 0 10px 0;
  align-items: center;
`;

export const CaixaBotoesInferInferiores = styled.View`
  width: 100%;
  flex-direction: row;
  padding: 0 75px;
`;

export const BotaoAgora = styled.TouchableOpacity`
  background: #ff7200;
  border-radius: 4px;
  align-items: center;
  padding: 10px 0;
  margin-bottom: 8px;

  width: 100%;
`;
export const TextBotaoAgora = styled.Text`
  font-size: 20px;
  color: #fff;
`;
