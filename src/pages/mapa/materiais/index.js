import React, {useState, useEffect} from 'react';
import {StyleSheet} from 'react-native';
import MapView, {Marker} from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation';
import {useNavigation} from '@react-navigation/native';

import {imagens} from '../../../util/enderecosImagens';
import ListMaterial from '../../../components/collaspe';

import {
  Container,
  CaixaTopo,
  TextoMaterias,
  CaixaConteudos,
  TracoTopo,
  ConteudoItens,
  CaixaBotoesInferInferiores,
  BotaoAgora,
  TextBotaoAgora,
} from './styles';

export default function MapaMateriais() {
  // const [loading, setLoading] = useState(true);
  const [coordinates, setCoordinates] = useState({
    longitude: -22.892218,
    latitude: -43.118249,
  });
  const [error, setError] = useState({});
  const navigation = useNavigation();

  useEffect(() => {
    Geolocation.getCurrentPosition(
      ({coords}) => {
        setCoordinates(coords);
        // setLoading(false);
      },
      error => {
        setError({error: error.message});
      },
      {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000},
    );
  }, []);
  const style = StyleSheet.create({
    mapa: {
      width: '100%',
      height: 400,
    },
  });

  function renderPoits() {
    return (
      <Marker
        coordinate={{
          latitude: coordinates.latitude,
          longitude: coordinates.longitude,
        }}
        title="Minha localização"
      />
    );
  }

  return (
    <Container>
      <MapView
        style={style.mapa}
        initialRegion={{
          latitude: coordinates.latitude,
          longitude: coordinates.longitude,
          latitudeDelta: 0.0068,
          longitudeDelta: 0.0068,
        }}>
        {renderPoits()}
      </MapView>
      <CaixaTopo>
        <CaixaConteudos>
          <TracoTopo />
          <TextoMaterias>Materiais disponibilizados</TextoMaterias>
          <ConteudoItens>
            <ListMaterial
              imagem={imagens.imgVidro}
              texto={'vidro'}
              styleText={styleText.vidro}
              styleImagem={styleImagem.vidro}
            />
            <ListMaterial
              imagem={imagens.imgPapel}
              texto={'papel'}
              styleText={styleText.papel}
              styleImagem={styleImagem.papel}
            />

            <ListMaterial
              imagem={imagens.imgPlastico}
              texto={'plastico'}
              styleText={styleText.plastico}
              styleImagem={styleImagem.plastico}
            />
            <ListMaterial
              imagem={imagens.imgAluminio}
              texto={'eletrônico'}
              styleText={styleText.eletronico}
              styleImagem={styleImagem.eletronico}
            />
          </ConteudoItens>
        </CaixaConteudos>

        <CaixaBotoesInferInferiores>
          <BotaoAgora onPress={() => navigation.navigate('TempoChagada')}>
            <TextBotaoAgora>Confirmar</TextBotaoAgora>
          </BotaoAgora>
        </CaixaBotoesInferInferiores>
      </CaixaTopo>
    </Container>
  );
}

const styleText = StyleSheet.create({
  vidro: {
    translateX: 18,
    translateY: 4,
    alignItems: 'center',
  },
  papel: {
    translateX: 21,
    translateY: 4,
    alignItems: 'center',
  },
  plastico: {
    translateX: 30,
    translateY: 4,
    alignItems: 'center',
  },
  eletronico: {
    translateX: 37,
    translateY: 4,
    alignItems: 'center',
  },
});
{
  /* {translateX: 120} */
}
const styleImagem = StyleSheet.create({
  vidro: {
    translateX: 120,
  },
  papel: {translateX: 120},
  plastico: {translateX: 110},
  eletronico: {translateX: 102},
});
