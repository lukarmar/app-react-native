import React, {useState, useEffect} from 'react';
import {View, ActivityIndicator, StyleSheet} from 'react-native';
import MapView, {Marker} from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation';
import {useNavigation} from '@react-navigation/native';
import Icone from 'react-native-vector-icons/Entypo';

import {imagens} from '../../../util/enderecosImagens';

import {
  Container,
  CaixaTopo,
  CaixaConteudos,
  TextoSaudacao,
  BarraAgendar,
  OndeColetar,
  EndConteudo,
  CaixaEnderecos,
  IconeCasa,
  TextAgendar,
  BotaoAgendarSuperior,
  Endereco,
  CepCidade,
  CaixaBotoesInferInferiores,
  BotaoAgendarInferior,
  TextBotaoAgendar,
  BotaoAgora,
  TextBotaoAgora,
} from './styles';

export default function MapainIcial() {
  const [loading, setLoading] = useState(true);
  const [coordinates, setCoordinates] = useState({latitude: 0, longitude: 0});
  const [error, setError] = useState({});
  const navigation = useNavigation();

  useEffect(() => {
    Geolocation.getCurrentPosition(
      ({coords}) => {
        setCoordinates(coords);
        setLoading(false);
      },
      error => {
        setError({error: error.message});
      },
      {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000},
    );
  }, []);
  const style = StyleSheet.create({
    mapa: {
      width: '100%',
      height: 400,
    },
  });

  function renderPoits() {
    return (
      <Marker
        coordinate={{
          latitude: coordinates.latitude,
          longitude: coordinates.longitude,
        }}
        title="Minha localização"
      />
    );
  }

  return (
    <Container>
      <MapView
        style={style.mapa}
        initialRegion={{
          latitude: coordinates.latitude,
          longitude: coordinates.longitude,
          latitudeDelta: 0.0068,
          longitudeDelta: 0.0068,
        }}>
        {renderPoits()}
      </MapView>
      <CaixaTopo>
        <CaixaConteudos>
          <TextoSaudacao>Bom dia Renato</TextoSaudacao>
          <BarraAgendar>
            <OndeColetar>Onde coletar?</OndeColetar>
            <BotaoAgendarSuperior
              onPress={() => navigation.navigate('AgendarColeta')}>
              <Icone name="stopwatch" size={25} />
              <TextAgendar>Agendar</TextAgendar>
            </BotaoAgendarSuperior>
          </BarraAgendar>

          <EndConteudo>
            <IconeCasa source={imagens.casaiCone} />
            <CaixaEnderecos>
              <Endereco>Rua Celso Bastos de Barros, 176 - Icarai</Endereco>
              <CepCidade>Niterói - RJ, 24220-121</CepCidade>
            </CaixaEnderecos>
          </EndConteudo>
        </CaixaConteudos>

        <CaixaBotoesInferInferiores>
          <BotaoAgora onPress={() => navigation.navigate('MapaMateriais')}>
            <TextBotaoAgora>Agora</TextBotaoAgora>
          </BotaoAgora>
          <BotaoAgendarInferior
            onPress={() => navigation.navigate('AgendarColeta')}>
            <TextBotaoAgendar>Agendar</TextBotaoAgendar>
          </BotaoAgendarInferior>
        </CaixaBotoesInferInferiores>
      </CaixaTopo>
    </Container>
  );
}
