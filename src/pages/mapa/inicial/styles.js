import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  align-items: center;
`;

export const CaixaTopo = styled.View`
  width: 100%;
  flex: 1;
  align-items: center;
  padding: 22px 14.5px 2px 14.5px;

  justify-content: space-between;
`;

export const CaixaConteudos = styled.View`
  width: 100%;
  align-items: center;
`;
export const TextoSaudacao = styled.Text`
  font-size: 20px;
  width: 100%;
  text-align: center;
  font-weight: bold;
  padding-bottom: 6px;
  margin-bottom: 3px;
  color: #000;

  border-bottom-width: 3px;
  border-bottom-color: #ff7200;
`;
export const BarraAgendar = styled.View`
  width: 100%;
  border-radius: 4px;
  background: #b9b9b9;
  padding: 5px 20px;
  margin: 8px 0 30px 0;

  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;
export const OndeColetar = styled.Text`
  font-size: 15px;
  font-weight: bold;
  color: #000;
`;
export const BotaoAgendarSuperior = styled.TouchableOpacity`
  flex-direction: row;
  background: #fff;
  border-radius: 10px;
  justify-content: center;
  align-items: center;
  width: 167px;
  padding: 7px 15px 7px 15px;
`;

export const TextAgendar = styled.Text`
  font-size: 15px;
  font-weight: bold;
  margin-left: 13px;
`;
export const EndConteudo = styled.View`
  width: 100%;
  flex-direction: row;
  padding: 0 0 10px 0;

  align-items: center;
  border-bottom-width: 1px;
  border-bottom-color: #ff7200;
`;
export const IconeCasa = styled.Image`
  width: 23px;
  height: 23px;
`;

export const CaixaEnderecos = styled.View`
  flex: 1;
  margin-left: 20px;
`;
export const Endereco = styled.Text`
  font-size: 15px;
  color: #000;
  text-align: left;
  font-weight: bold;
  letter-spacing: 0.8px;
`;
export const CepCidade = styled.Text`
  font-size: 12px;
  text-align: left;
  font-weight: bold;
  letter-spacing: 0.8px;
`;

export const CaixaBotoesInferInferiores = styled.View`
  width: 100%;
  flex-direction: row;
`;
export const BotaoAgendarInferior = styled.TouchableOpacity`
  align-items: center;
  width: 50%;
  padding: 10px 0;

  border-left-width: 1px;
  border-left-color: #ff7200;
`;
export const TextBotaoAgendar = styled.Text`
  font-size: 20px;
`;

export const BotaoAgora = styled.TouchableOpacity`
  border-right-width: 1px;
  border-right-color: #ff7200;
  align-items: center;
  padding: 10px 0;

  width: 50%;
`;
export const TextBotaoAgora = styled.Text`
  font-size: 20px;
`;
