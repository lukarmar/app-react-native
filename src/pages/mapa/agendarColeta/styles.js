import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  align-items: center;
`;

export const CaixaConteudos = styled.View`
  width: 100%;
  padding: 18px 10px 0 10px;
  align-items: center;
`;
export const TracoTopo = styled.View`
  width: 100%;
  margin: 10px 0 3px 0;

  border-bottom-width: 3px;
  border-bottom-color: #ff7200;
`;

export const TextoMaterias = styled.Text`
  font-size: 18px;
  font-weight: bold;
`;
export const CaixaTempo = styled.View`
  width: 100%;
  padding: 0 10px;
`;
export const CaixaData = styled.View`
  width: 100%;
  padding: 30px 0;
  border-bottom-width: 1px;
  border-bottom-color: #ff7200;
`;

export const CaixaHorario = styled.View`
  width: 100%;
  padding: 30px 0;
  border-bottom-width: 1px;
  border-bottom-color: #ff7200;
`;

export const CaixaBotoesInferInferiores = styled.View`
  flex: 1;
  width: 100%;

  justify-content: flex-end;
  padding: 0 10px;
`;

export const BotaoAgora = styled.TouchableOpacity`
  background: #ff7200;
  border-radius: 4px;
  align-items: center;
  padding: 15px 0;
  margin-bottom: 8px;
`;
export const TextBotaoAgora = styled.Text`
  font-size: 20px;
  color: #fff;
`;
