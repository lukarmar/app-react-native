import React, {useState, useEffect} from 'react';
import {StyleSheet} from 'react-native';
import MapView, {Marker} from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation';

import ModuloTempo from '../../../components/tempoAgendamento';

import {imagens} from '../../../util/enderecosImagens';

import {
  Container,
  TextoMaterias,
  CaixaConteudos,
  CaixaData,
  CaixaHorario,
  CaixaTempo,
  TracoTopo,
  CaixaBotoesInferInferiores,
  BotaoAgora,
  TextBotaoAgora,
} from './styles';

export default function AgendarColeta() {
  // const [loading, setLoading] = useState(true);
  const [coordinates, setCoordinates] = useState({
    longitude: -22.892218,
    latitude: -43.118249,
  });
  const [error, setError] = useState({});

  useEffect(() => {
    Geolocation.getCurrentPosition(
      ({coords}) => {
        setCoordinates(coords);
        // setLoading(false);
      },
      error => {
        setError({error: error.message});
      },
      {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000},
    );
  }, []);
  const style = StyleSheet.create({
    mapa: {
      width: '100%',
      height: 410,
    },
  });

  function renderPoits() {
    return (
      <Marker
        coordinate={{
          latitude: coordinates.latitude,
          longitude: coordinates.longitude,
        }}
        title="Minha localização"
      />
    );
  }

  return (
    <Container>
      <MapView
        style={style.mapa}
        initialRegion={{
          latitude: coordinates.latitude,
          longitude: coordinates.longitude,
          latitudeDelta: 0.0068,
          longitudeDelta: 0.0068,
        }}>
        {renderPoits()}
      </MapView>

      <CaixaConteudos>
        <TextoMaterias>Agendar Coleta</TextoMaterias>
        <TracoTopo />
      </CaixaConteudos>
      <CaixaTempo>
        <CaixaData>
          <ModuloTempo mode={'date'} imagem={imagens.imgCalendario} />
        </CaixaData>
        <CaixaHorario>
          <ModuloTempo mode={'time'} imagem={imagens.imgRelogio} />
        </CaixaHorario>
      </CaixaTempo>

      <CaixaBotoesInferInferiores>
        <BotaoAgora>
          <TextBotaoAgora>Definir horário</TextBotaoAgora>
        </BotaoAgora>
      </CaixaBotoesInferInferiores>
    </Container>
  );
}
