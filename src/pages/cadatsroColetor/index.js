import React, {useState} from 'react';
import {ScrollView} from 'react-native';
import {Label, Text} from 'native-base';
import {useNavigation} from '@react-navigation/native';
import {TextInputMask} from 'react-native-masked-text';

import {imagens} from '../../util/enderecosImagens';
import InputCad from '../../components/inputCadastro';
import DatePicker from '../../components/inputData';
import InputCpfCnpf from '../../components/inputCpfCnpj';
import Teste from './labelAnimada';

import {
  Container,
  Titulo,
  Aviso,
  Formulario,
  InputCadastro,
  ImagemRecycle,
  ItemInput,
  CaixaBotaoInscricao,
  BotaoInscricao,
} from './styles';

export default function CadastroFornecedor() {
  const navigation = useNavigation();

  return (
    <ScrollView keyboardShouldPersistTaps="handled">
      <Container>
        <Titulo>Crie seu perfil</Titulo>
        <Aviso>
          Isto lhe dará um local para armazenar todas as informações sobre o
          quanto você já ajudou a natureza e ajudará seus amigos a encontrarem
          você.
        </Aviso>
        <Formulario>
          <ItemInput floatingLabel>
            <Label>Nome</Label>
            <InputCadastro autoCorrect={false} autoCapitalize="none" />
          </ItemInput>
          <ItemInput floatingLabel>
            <Label>Sobrenome</Label>
            <TextInputMask />
          </ItemInput>
          <Teste />
          <DatePicker />
          <InputCad />
          <InputCpfCnpf />
        </Formulario>
        <CaixaBotaoInscricao>
          <BotaoInscricao onPress={() => navigation.navigate('EntarContato')}>
            Concordar e Inscrever-se
          </BotaoInscricao>
        </CaixaBotaoInscricao>
        <ImagemRecycle source={imagens.RecycleDinnome} />
      </Container>
    </ScrollView>
  );
}
