import styled from 'styled-components/native';
import {Item, Input} from 'native-base';

import Botao from '../../components/Button';

export const Container = styled.View`
  flex: 1;
  align-items: center;

  background: #fff;
  padding: 25px 15px 0 15px;
`;
export const Titulo = styled.Text`
  font-size: 23px;
  color: #000;
  font-weight: bold;
  align-self: flex-start;
`;
export const Aviso = styled.Text`
  font-size: 15px;
  margin: 13px 0 30px;
  letter-spacing: 1px;
  color: #000;
  font-weight: 600;
  text-align: left;
`;

export const Formulario = styled.View`
  width: 100%;
`;

export const InputCadastro = styled(Input)``;

export const ItemInput = styled(Item)`
  margin-bottom: 10px;
`;

export const ImagemRecycle = styled.Image`
  width: 161px;
  height: 50px;
`;

export const BotaoInscricao = styled(Botao)``;

export const CaixaBotaoInscricao = styled.View`
  width: 100%;
  margin: 25px 0 35px 0;
  padding: 0 35px 0 35px;
`;
