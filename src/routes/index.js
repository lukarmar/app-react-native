import React from 'react';
import {TouchableOpacity} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import Icone from 'react-native-vector-icons/MaterialIcons';

import Autenticacao from '../pages/autenticacao';
import EscolhaPerfil from '../pages/escolhaPerfil';

import TermoUso from '../pages/termos/termpUso';
import PoliticaPrivacidade from '../pages/termos/politicaPrivacidade';

import CadastroFornecedor from '../pages/cadastroFornecedor';
import CadastroColetor from '../pages/cadatsroColetor';

import EntarContato from '../pages/finalizandoCadastro/entrarContato';
import NormasComunidade from '../pages/finalizandoCadastro/normasComunidade';
import ProntoComecar from '../pages/finalizandoCadastro/prontoComecar';
import PermitirAcesso from '../pages/finalizandoCadastro/permitirAcesso';

import MapaInicial from '../pages/mapa/inicial';
import MapaMateriais from '../pages/mapa/materiais';
import TempoChegada from '../pages/mapa/tempoChegada';
import AgendarColeta from '../pages/mapa/agendarColeta';

const Stack = createStackNavigator();

export default function Rotas() {
  return (
    <Stack.Navigator initialRouteName="EscolhaPerfil">
      <Stack.Screen
        name="EscolhaPerfil"
        component={EscolhaPerfil}
        options={{headerShown: false, cardStyle: {backgroundColor: '#fff'}}}
      />
      <Stack.Screen
        name="Autenticacao"
        component={Autenticacao}
        options={{headerShown: false, cardStyle: {backgroundColor: '#fff'}}}
      />

      <Stack.Screen
        name="TermoUso"
        component={TermoUso}
        options={({navigation}) => ({
          cardStyle: {backgroundColor: '#fff'},
          headerStyle: {
            backgroundColor: '#FF7200',
            height: 78,
          },
          headerTintColor: '#fff',
          headerTitleContainerStyle: {marginLeft: -28},
          headerTitle: 'Termos de uso',
          headerLeft: () => (
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <Icone
                name="chevron-left"
                size={46}
                color="#fff"
                style={{marginLeft: -4}}
              />
            </TouchableOpacity>
          ),
        })}
      />
      <Stack.Screen
        name="PoliticaPrivacidade"
        component={PoliticaPrivacidade}
        options={({navigation}) => ({
          cardStyle: {backgroundColor: '#fff'},
          headerStyle: {backgroundColor: '#FF7200', height: 78},
          headerTintColor: '#fff',
          headerTitleContainerStyle: {marginLeft: -28},
          headerTitle: 'Política de privacidade',
          headerLeft: () => (
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <Icone
                name="chevron-left"
                size={46}
                color="#fff"
                pressColorAndroid="#f0ff"
                style={{marginLeft: -4}}
              />
            </TouchableOpacity>
          ),
        })}
      />

      <Stack.Screen
        name="CadastroFornecedor"
        component={CadastroFornecedor}
        options={({navigation}) => ({
          cardStyle: {backgroundColor: '#fff'},
          headerStyle: {
            backgroundColor: '#FF7200',
            height: 78,
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontSize: 18,
          },
          headerTitleContainerStyle: {marginLeft: -28},
          headerTitle: 'Inscreva-se',
          headerLeft: () => (
            <Icone
              name="chevron-left"
              size={46}
              color="#fff"
              pressColorAndroid="#f0ff"
              style={{marginLeft: -4}}
              onPress={() => navigation.goBack()}
            />
          ),
        })}
      />
      <Stack.Screen
        name="CadastroColetor"
        component={CadastroColetor}
        options={({navigation}) => ({
          cardStyle: {backgroundColor: '#fff'},
          headerStyle: {
            backgroundColor: '#FF7200',
            height: 78,
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontSize: 18,
          },
          headerTitleContainerStyle: {marginLeft: -28},
          headerTitle: 'Inscreva-se',
          headerLeft: () => (
            <Icone
              name="chevron-left"
              size={46}
              color="#fff"
              pressColorAndroid="#f0ff"
              style={{marginLeft: -4}}
              onPress={() => navigation.goBack()}
            />
          ),
        })}
      />
      <Stack.Screen
        name="EntarContato"
        component={EntarContato}
        options={() => ({
          cardStyle: {backgroundColor: '#fff'},
          headerStyle: {
            backgroundColor: '#FF7200',
            height: 78,
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontSize: 18,
          },

          headerTitle: 'Perfil',
          headerLeft: ({disabled}) => {
            disabled: true;
          },
        })}
      />
      <Stack.Screen
        name="NormasComunidade"
        component={NormasComunidade}
        options={() => ({
          cardStyle: {backgroundColor: '#fff'},
          headerStyle: {
            backgroundColor: '#FF7200',
            height: 78,
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontSize: 18,
          },

          headerTitle: 'Perfil',
          headerLeft: ({disabled}) => {
            disabled: true;
          },
        })}
      />
      <Stack.Screen
        name="ProntoComecar"
        component={ProntoComecar}
        options={() => ({
          cardStyle: {backgroundColor: '#fff'},
          headerStyle: {
            backgroundColor: '#FF7200',
            height: 78,
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontSize: 18,
          },

          headerTitle: 'Perfil',
          headerLeft: ({disabled}) => {
            disabled: true;
          },
        })}
      />
      <Stack.Screen
        name="PermitirAcesso"
        component={PermitirAcesso}
        options={() => ({
          headerShown: false,
          cardStyle: {backgroundColor: '#fff'},
        })}
      />
      <Stack.Screen
        name="MapaInicial"
        component={MapaInicial}
        options={() => ({
          headerShown: false,
          cardStyle: {backgroundColor: '#fff'},
        })}
      />
      <Stack.Screen
        name="MapaMateriais"
        component={MapaMateriais}
        options={() => ({
          headerShown: false,
          cardStyle: {backgroundColor: '#fff'},
        })}
      />
      <Stack.Screen
        name="TempoChagada"
        component={TempoChegada}
        options={() => ({
          headerShown: false,
          cardStyle: {backgroundColor: '#fff'},
        })}
      />
      <Stack.Screen
        name="AgendarColeta"
        component={AgendarColeta}
        options={() => ({
          headerShown: false,
          cardStyle: {backgroundColor: '#fff'},
        })}
      />
    </Stack.Navigator>
  );
}
